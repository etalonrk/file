#/bin/bash

rm -rf public
rm files.md

files=$(ls index)

mkdir public

echo $files > files
sed -E -e 's/[[:blank:]]+/\n/g' files > files2
awk '$0="[insert]("$0' files2 > files3
sed 's/$/\)/g' files3 > files4
sed "s/)/)\n\n---\n/g" files4 > files5
sed "s/(/(index\//g" files5 > files6

rm files files2 files3 files4 files5
mv files6 files.md

for insert in $files
do
sed "0,/insert/{s/insert/$insert/}" files.md > files2.md
rm files.md
mv files2.md files.md
done

perl markdown.pl files.md > files.html

cat template.html > index.html
cat files.html >> index.html
echo "</body>" >> index.html
echo "</html>" >> index.html
