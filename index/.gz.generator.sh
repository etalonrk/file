#/bin/bash

tar -cvzf austall.tar.gz $work/austall/
tar -cvzf rss.generator.tar.gz $work/feed.rss/
tar -cvzf files.tar.gz $work/files/
tar -cvzf homepages.tar.gz $work/homepages/
tar -cvzf wallpapers.tar.gz $work/wallpapers/
tar -cvzf website.tar.gz $work/website/
